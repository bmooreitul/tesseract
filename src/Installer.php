<?php

namespace Itul\Tesseract;

class Installer{
    
    public static function installTesseract(){

        if(!realpath(base_path()."/../conda/bin")){
            $commands = $commands+[
                "curl https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh > ~/Miniconda.sh",
                "bash ~/Miniconda.sh -b -p ~/conda",
                "rm -f ~/Miniconda.sh",
            ];
        }

        if(!realpath(base_path()."/../conda/bin/tesseract")) $commands = $commands+["~/conda/bin/conda install -y -c conda-forge tesseract"];
        if(!realpath(base_path()."/../conda/bin/pdftotext")) $commands = $commands+["~/conda/bin/conda install -y -c conda-forge poppler"];

        @exec($commands);

        /*
        if(file_exists(realpath(base_path()."/../.bashrc"))){
            $content = file_get_contents(realpath(base_path()."/../.bashrc"));
            if(strpos($content, "alias tesseract=") === false){
                $content .= "\n"."alias tesseract='~/conda/bin/tesseract';";
                file_put_contents(realpath(base_path()."/../.bashrc"), $content);
            }
        }
        */

        /*
        $commands = implode(" ; ", [
            "curl https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh > ~/Miniconda.sh",
            "bash ~/Miniconda.sh -b -p ~/conda",
            "~/conda/bin/conda install -y -c conda-forge tesseract",
            "rm -f ~/Miniconda.sh",
            "export TESSDATA_PREFIX='~/conda/share/tessdata'",
            "cp osd.traineddata ~/osd.traineddata"
            //"wget https://github.com/tesseract-ocr/tessdata/raw/main/eng.traineddata",
            //"mv -v eng.traineddata ~/conda/share/tessdata/eng.traineddata",
        ]);

        @exec($commands);

        if(file_exists(realpath(base_path()."/../.bashrc"))){
            $content = file_get_contents(realpath(base_path()."/../.bashrc"));
            if(strpos($content, "alias tesseract=") === false){
                $content .= "\n"."alias tesseract='~/conda/bin/tesseract';";
                file_put_contents(realpath(base_path()."/../.bashrc"), $content);
            }
        }
        */
    }
}