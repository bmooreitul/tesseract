<?php

namespace Itul\Tesseract;

class Tesseract{

    public function toSearchablePdf($files){

        if(is_array($files)){
            $ret = [];
            foreach($files as $file) $ret[] = $this->toSearchablePdf($this->searchablePdf((new Itul\PdfTools\PdfTools)->toPdf($file)));
            return $ret;
        }

        return (new \thiagoalessio\TesseractOCR\TesseractOCR($files))->pdf()->run();
    }
}
